/*!
 * Site Specific IU Search Init
 */
(function (window, document, $, undefined) {

    $(document).ready(function() {

        var IUSearch = window.IUSearch || {};

        var searchOptions = {
            CX: {
        		site: '004156415053543418472:j5r2hj-hpgu', // IUAA
        		all: '016278320858612601979:ddl1l9og1w8' // IU
                //all: '016278320858612601979:w_w23ggyftu' // IU Bloomington
                //all: '016278320858612601979:pwcza8pis6k' // IUPUI
            },
            wrapClass: 'row pad',
            searchBoxIDs: ['search', 'off-canvas-search']
        }

        /* Initialize global search */
        IUSearch.init && IUSearch.init( searchOptions );
    });

})(window, window.document, jQuery);

/*!
 * Overrides
 */
;(function ($, window, document, undefined ) {

    'use strict';

    IU.addHelper('externalLinksInNewTabs', function() {

        IU.debug('Helper: External Links (overriden)');

        var scope = this;

        $('a:not(.intent, .email, .external, [href^="tel"])').each(function() {
            var a = new RegExp('/' + window.location.host + '/');

            if (!a.test(this.href)) {

                $(this).addClass('external');

                $(this).on('click', function(event) {
                    event.stopPropagation();
                    event.preventDefault();
                    window.open(this.href, '_blank');
                });
            }
        });

        scope.debug('Helper: Open external links in new tabs (overriden)');
    });
})( jQuery, window, window.document );

/*!
 * IU Equal Heights
 * v3.0
 * Dynamically sets element heights; Useful in grids
 */
;(function ($, window, document, undefined ) {

    'use strict';

    // add initialisation
    IU.addInitalisation('equal-heights', function() {

        IU.debug('Module: Equal Heights (overriden)');

        var font = new FontFaceObserver('BentonSansRegular');

        var $items = $(".grid > .grid-item");

        font.load().then(function () {
            // console.log('Font is available');
            $items.matchHeight();
        });

        // http://stackoverflow.com/questions/6781031/use-jquery-css-to-find-the-tallest-of-all-elements
        // var $content = $items.find('.feature .content');

        var rows = $.fn.matchHeight._rows($('.grid-item'));

        setTimeout( function() {
            $.each(rows, function(i, row) {
                // row.first().addClass('first');
                // row.last().addClass('last');

                var $content = row.find('.feature .content');
                // console.log($content);
                var $buttons = row.find('.button');
                // console.log($buttons);

                var buttonHeights = $buttons.map(function() {
                    // console.log( $(this).text(), $(this).height() );
                    return $(this).outerHeight();
                }).get();

                // console.log(buttonHeights);

                var maxHeight = Math.max.apply(null, buttonHeights);

                $content.css({ "paddingBottom" : maxHeight });
                $content.matchHeight();
            });

        }, 100);




    });

    // Register UI module
    IU.UIModule({
        module: 'equal-heights',
        init: function() {
            IU.initialize('equal-heights');
        }
    });

})( jQuery, window, window.document );


/*!
 * Site Specific JS
 */
(function (window, document, $, undefined) {

    $(document).ready(function() {

        /**
         * Set various Foundation defaults
         */
        // Foundation.OffCanvas.defaults.transitionTime = 500;
        // Foundation.OffCanvas.defaults.forceTop = false;
        // Foundation.OffCanvas.defaults.positiong = 'right';

        // Foundation.Accordion.defaults.multiExpand = true;
        Foundation.Accordion.defaults.allowAllClosed = true;

        /**
         * Initialize Foundation
         */
        $(document).foundation();

        /**
         * Load global IU object
         */
        var IU = window.IU || {};

        /**
         * Delete modules if necessary (prevents them from auto-initializing)
         */
        // delete IU.uiModules['accordion'];

        /**
         * Initialize global IUComm & its modules
         * Custom settings can be passsed here
         */
        IU.init && IU.init({
            debug: true
            // accordion: {needsMarkup: true}
        });


    });

})(window, window.document, jQuery);